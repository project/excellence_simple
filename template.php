<?php

/**
 * @file template.php
 */
require_once ("helper_methods.php");
require_once ("common_methods.php");
global $language;
if (isset($language)) {
  $language->direction = LANGUAGE_LTR;
}
drupal_static_reset('element_info');
/**
 * Page alter.
 */
function excellence_simple_page_alter($page) {
  $mobileoptimized = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array('name' => 'MobileOptimized','content' => 'width'));
  $handheldfriendly = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array('name' => 'HandheldFriendly','content' => 'true'));
  $viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array('name' => 'viewport','content' => 'width=device-width, initial-scale=1'));
  drupal_add_html_head($mobileoptimized,'MobileOptimized');
  drupal_add_html_head($handheldfriendly,'HandheldFriendly');
  drupal_add_html_head($viewport,'viewport');
}

/**
 * Override or insert variables into the page template.
 */
function excellence_simple_process_html(&$variables) {
  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}

/**
 * Preprocess variables for html.tpl.php
 */
function excellence_simple_preprocess_html(&$variables) { 
  /**
   * Add IE8 Support
   */
  drupal_add_css(path_to_theme() . '/css/ie8.css',array(
    'group' => CSS_THEME,
    'browsers' => array('IE' => '(lt IE 9)','!IE' => false),
    'preprocess' => false));
  // jquery added via jquery update set to 2.1 version
  drupal_add_css(path_to_theme() . '/css/bootstrap.min.css', array('group' => CSS_THEME, 'every_page' => true)); 
  drupal_add_js(path_to_theme() . '/js/bootstrap.min.js', array('group' => JS_THEME, 'every_page' => TRUE));
  drupal_add_js(path_to_theme(). '/js/scripts.tools.js',array('scope' => 'footer'));
}

/**
 * Preprocess variables for page template.
 */
function excellence_simple_preprocess_page(&$variables) {

  if (drupal_is_front_page()) {
    //drupal_add_css(path_to_theme() . '/style.front.css', 'theme');
    //$variables['styles'] = drupal_get_css();
    $variables['theme_hook_suggestions'][] = 'page__front';
  } elseif (!drupal_is_front_page() && isset($variables['node'])) {
    // If the content type's machine name is "my_machine_name" the file
    // name will be "page--my-machine-name.tpl.php".
    $variables['theme_hook_suggestions'][] = "page__" . $variables['node']->type;
  } else {
    $variables['theme_hook_suggestions'][] = 'page';
  }
  
  if ($variables['page']['sidebar_first'] && $variables['page']['sidebar_second']) {
    $variables['sidebar_grid_class'] = 'sidebar-grid col-md-3';
    $variables['main_grid_class'] = 'main-grid col-md-6';
  } elseif ($variables['page']['sidebar_first'] || $variables['page']['sidebar_second']) {
    $variables['sidebar_grid_class'] = 'sidebar-grid col-md-4';
    $variables['main_grid_class'] = 'main-grid col-md-8';
  } else {
    $variables['main_grid_class'] = 'main-grid col-md-12';
  }
  if ($variables['page']['header_top_left'] && $variables['page']['header_top_right']) {
    $variables['header_top_left_grid_class'] = 'header-grid col-md-8';
    $variables['header_top_right_grid_class'] = 'header-grid col-md-4';
  } elseif ($variables['page']['header_top_right'] || $variables['page']['header_top_left']) {
    $variables['header_top_left_grid_class'] = 'header-grid col-md-12';
    $variables['header_top_right_grid_class'] = 'header-grid col-md-12';
  }
  $variables['primary_nav'] = false;
  if ($variables['main_menu']) {
    // Build links.
    $variables['primary_nav'] = menu_tree(variable_get('menu_main_links_source','main-menu'));
    // Provide default theme wrapper function.
    $variables['primary_nav']['#theme_wrappers'] = array('menu_tree__primary');
  }
  // Secondary nav.
  $variables['secondary_nav'] = false;
  if ($variables['secondary_menu']) {
    // Build links.
    $variables['secondary_nav'] = menu_tree(variable_get('menu_secondary_links_source','user-menu'));
    // Provide default theme wrapper function.
    $variables['secondary_nav']['#theme_wrappers'] = array('menu_tree__secondary');
  }
  // Add search box variable.
  $search_box = '';
  if (module_exists('search')) {
    $search_form = drupal_get_form('search_form');
    $search_box = drupal_render($search_form);
    $variables['search_box'] = $search_box;
  }
}
/**
 * Preprocess variables for block.tpl.php
 */
function excellence_simple_preprocess_block(&$variables) {
  $variables['classes_array'][] = 'clearfix';
  if ($variables['block_html_id'] == 'block-system-main') {
    $variables['theme_hook_suggestions'][] = 'block__no_wrapper';
  }
  $variables['title_attributes_array']['class'][] = 'block-title';
}
/**
 * Overrides theme_breadcrumb().
 */
function excellence_simple_breadcrumb($variables) {
  $output = '';
  $breadcrumb = $variables['breadcrumb'];
  // Determine if we are to display the breadcrumb.
  $breadcrumb_status = theme_get_setting('excellence_breadcrumb_display');
  if (($breadcrumb_status == 1 || ($breadcrumb_status == 2 && arg(0) == 'admin')) && !empty($breadcrumb)) {
    $output = theme('item_list', array(
      'attributes' => array(
        'class' => array('breadcrumb'),
      ),
      'items' => $breadcrumb,
      'type' => 'ol',
    ));
  }
  return $output;
}

/**
 * Implements hook_preprocess_breadcrumb().
 */
function excellence_simple_preprocess_breadcrumb(&$variables) {
  $breadcrumb = &$variables['breadcrumb'];

  // Optionally get rid of the homepage link.
  $show_breadcrumb_home = theme_get_setting('excellence_breadcrumb_home');
  if (!$show_breadcrumb_home) {
    array_shift($breadcrumb);
  }

  if (theme_get_setting('excellence_breadcrumb_title') && !empty($breadcrumb)) {
    $item = menu_get_item();

    $page_title = !empty($item['tab_parent']) ? check_plain($item['title']) : drupal_get_title();
    if (!empty($page_title)) {
      $breadcrumb[] = array(
        // If we are on a non-default tab, use the tab's title.
        'data' => $page_title,
        'class' => array('active'),
      );
    }
  }
}
