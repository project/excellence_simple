<?php
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function excellence_simple_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['excellence'] = array(
    '#type' => 'vertical_tabs',
    '#attached' => array(
      'js'  => array(drupal_get_path('theme', 'excellence_simple') . '/js/scripts.admin.js'),
    ),
    '#prefix' => '<h2><small>' . t('Excellence Settings') . '</small></h2>',
    '#weight' => -10,
   );
    $form['setexcellence'] = array(
        '#type' => 'fieldset',
        '#title' => t('Excellence Business Theme Settings'),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
		'#weight' => -10,
    );

    $form['setexcellence']['tabs'] = array(
        '#type' => 'vertical_tabs',
    );

    $form['setexcellence']['tabs']['basic_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Basic Settings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );

    $form['setexcellence']['tabs']['basic_settings']['excellence_breadcrumb_display'] = array(
        '#type' => 'select',
        '#title' => t('Breadcrumb visibility'),
        '#description'   => t('Use the checkbox to enable or disable the breadcrumb.'),
        '#default_value' => theme_get_setting('excellence_breadcrumb_display','excellence_simple'),
		'#options' => array(
		  0 => t('Hidden'),
		  1 => t('Visible'),
		  2 => t('Only in admin areas'),
		),
    );
    $form['setexcellence']['tabs']['basic_settings']['excellence_breadcrumb_home'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show "Home" breadcrumb link'),
    '#default_value' => theme_get_setting('excellence_breadcrumb_home', 'excellence_simple'),
    '#description' => t('If your site has a module dedicated to handling breadcrumbs already, ensure this setting is enabled.'),
    '#states' => array(
      'invisible' => array(
        ':input[name="bootexcellence_breadcrumb"]' => array('value' => 0),
      ),
    ),
  );  
  $form['setexcellence']['tabs']['basic_settings']['excellence_breadcrumb_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show current page title at end'),
    '#default_value' => theme_get_setting('excellence_breadcrumb_title', 'excellence_simple'),
    '#description' => t('If your site has a module dedicated to handling breadcrumbs already, ensure this setting is disabled.'),
    '#states' => array(
      'invisible' => array(
        ':input[name="bootexcellence_breadcrumb"]' => array('value' => 0),
      ),
    ),
  );
    
    $form['setexcellence']['tabs']['behaviors'] = array(
        '#type' => 'fieldset',
        '#title' => t('Behaviors'),
        '#group' => 'bootexcellence',
    );
    $form['setexcellence']['tabs']['behaviors']['excellence_alert_dismissible'] = array(
      '#type' => 'checkbox',
      '#title' => t('Dismissible Alert'),
      '#description' => t("Makes the alert messages dismissable."),
      '#default_value' => theme_get_setting('excellence_alert_dismissible', 'excellence_simple'),
    );
    $form['setexcellence']['tabs']['behaviors']['excellence_searchform_header'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show search box in header'),
        '#description'   => t('Use the search to enable header page.'),
        '#default_value' => theme_get_setting('excellence_searchform_header', 'excellence_simple'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );

	  // Forms
  $form['setexcellence']['tabs']['behaviors']['excellence_forms_required_has_error'] = array(
    '#type' => 'checkbox',
    '#title' => t('Make required elements display as an error'),
    '#default_value' => theme_get_setting('excellence_forms_required_has_error', 'excellence_simple'),
    '#description' => t('If an element in a form is required, enabling this will always display the element with a <code>.has-error</code> class. This turns the element red and helps in usability for determining which form elements are required to submit the form.  This feature compliments the "JavaScript > Forms > Automatically remove error classes when values have been entered" feature.'),
  );
  $form['setexcellence']['tabs']['behaviors']['excellence_forms_smart_descriptions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Smart form descriptions (via Tooltips)'),
    '#description' => t('Convert descriptions into tooltips (must be enabled) automatically based on certain criteria. This helps reduce the, sometimes unnecessary, amount of noise on a page full of form elements.'),
    '#default_value' => theme_get_setting('excellence_forms_smart_descriptions', 'excellence_simple'),
  );
  $form['setexcellence']['tabs']['behaviors']['excellence_forms_smart_descriptions_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('"Smart form descriptions" maximum character limit'),
    '#description' => t('Prevents descriptions from becoming tooltips by checking the character length of the description (HTML is not counted towards this limit). To disable this filtering criteria, leave an empty value.'),
    '#default_value' => theme_get_setting('excellence_forms_smart_descriptions_limit', 'excellence_simple'),
    '#states' => array(
      'visible' => array(
        ':input[name="bootexcellence_forms_smart_descriptions"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['setexcellence']['tabs']['behaviors']['excellence_forms_smart_descriptions_allowed_tags'] = array(
    '#type' => 'textfield',
    '#title' => t('"Smart form descriptions" allowed (HTML) tags'),
    '#description' => t('Prevents descriptions from becoming tooltips by checking for HTML not in the list above (i.e. links). Separate by commas. To disable this filtering criteria, leave an empty value.'),
    '#default_value' => theme_get_setting('excellence_forms_smart_descriptions_allowed_tags', 'excellence_simple'),
    '#states' => array(
      'visible' => array(
        ':input[name="bootexcellence_forms_smart_descriptions"]' => array('checked' => TRUE),
      ),
    ),
  );
    $form['setexcellence']['tabs']['somecomponents'] = array(
        '#type' => 'fieldset',
        '#title' => t('Components'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );

    $form['setexcellence']['tabs']['somecomponents']['buttons']['excellence_button_size'] = array(
        '#type' => 'select',
        '#title' => t('Default button size'),
        '#default_value' => theme_get_setting('excellence_button_size','excellence_simple'),
        '#empty_option' => t('Normal'),
		'#options' => array(
		  'btn-xs' => t('Extra Small'),
		  'btn-sm' => t('Small'),
		  'btn-lg' => t('Large'),
		),
    );
    $form['setexcellence']['tabs']['somecomponents']['buttons']['excellence_button_colorize'] = array(
	  '#type' => 'checkbox',
      '#title' => t('Colorize Buttons'),
      '#default_value' => theme_get_setting('excellence_button_colorize','excellence_simple'),
	  '#description' => t('Adds classes to buttons based on their text value'),
	);
	$form['setexcellence']['tabs']['somecomponents']['buttons']['excellence_button_iconize'] = array(
    '#type' => 'checkbox',
    '#title' => t('Iconize Buttons'),
    '#default_value' => theme_get_setting('excellence_button_iconize','excellence_simple'),
    '#description' => t('Adds icons to buttons based on the text value'),
  );
  $form['setexcellence']['tabs']['somecomponents']['images']['excellence_image_shape'] = array(
    '#type' => 'select',
    '#title' => t('Default image shape'),
    '#description' => t('Add classes to an <code>&lt;img&gt;</code> element to easily style images in any project. Note: Internet Explorer 8 lacks support for rounded corners.'),
    '#default_value' => theme_get_setting('excellence_image_shape', 'excellence_simple'),
    '#empty_option' => t('None'),
    '#options' => array(
      'img-rounded' => t('Rounded'),
      'img-circle' => t('Circle'),
      'img-thumbnail' => t('Thumbnail'),
    ),
  );
  $form['setexcellence']['tabs']['somecomponents']['images']['excellence_image_responsive'] = array(
    '#type' => 'checkbox',
    '#title' => t('Responsive Images'),
    '#default_value' => theme_get_setting('excellence_image_responsive', 'excellence_simple'),
    '#description' => t('Images in Bootstrap 3 can be made responsive-friendly via the addition of the <code>.img-responsive</code> class. This applies <code>max-width: 100%;</code> and <code>height: auto;</code> to the image so that it scales nicely to the parent element.'),
  );
  $form['setexcellence']['tabs']['somecomponents']['excellence_table_bordered'] = array(
    '#type' => 'checkbox',
    '#title' => t('Bordered table'),
    '#default_value' => theme_get_setting('excellence_table_bordered', 'excellence_simple'),
    '#description' => t('Add borders on all sides of the table and cells.'),
  );
  $form['setexcellence']['tabs']['somecomponents']['excellence_table_condensed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Condensed table'),
    '#default_value' => theme_get_setting('excellence_table_condensed', 'excellence_simple'),
    '#description' => t('Make tables more compact by cutting cell padding in half.'),
  );
  $form['setexcellence']['tabs']['somecomponents']['excellence_table_hover'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hover rows'),
    '#default_value' => theme_get_setting('excellence_table_hover', 'excellence_simple'),
    '#description' => t('Enable a hover state on table rows.'),
  );
  $form['setexcellence']['tabs']['somecomponents']['excellence_table_striped'] = array(
    '#type' => 'checkbox',
    '#title' => t('Striped rows'),
    '#default_value' => theme_get_setting('excellence_table_striped', 'excellence_simple'),
    '#description' => t('Add zebra-striping to any table row within the <code>&lt;tbody&gt;</code>. <strong>Note:</strong> Striped tables are styled via the <code>:nth-child</code> CSS selector, which is not available in Internet Explorer 8.'),
  );
  $form['setexcellence']['tabs']['somecomponents']['excellence_table_responsive'] = array(
    '#type' => 'checkbox',
    '#title' => t('Responsive tables'),
    '#default_value' => theme_get_setting('excellence_table_responsive', 'excellence_simple'),
    '#description' => t('Makes tables responsive by wrapping them in <code>.table-responsive</code> to make them scroll horizontally up to small devices (under 768px). When viewing on anything larger than 768px wide, you will not see any difference in these tables.'),
  );
  
  $form['setexcellence']['tabs']['othercomponents'] = array(
        '#type' => 'fieldset',
        '#title' => t('Others omponents'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );
  $form['setexcellence']['tabs']['othercomponents']['popovers']['excellence_popover_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable popovers.'),
    '#description' => t('Elements that have the !code attribute set will automatically initialize the popover upon page load. !warning', array(
      '!code' => '<code>data-toggle="popover"</code>',
      '!warning' => '<strong class="error text-error">WARNING: This feature can sometimes impact performance. Disable if pages appear to "hang" after initial load.</strong>',
    )),
    '#default_value' => theme_get_setting('excellence_popover_enabled', 'excellence_simple'),
  );
  $form['setexcellence']['tabs']['othercomponents']['popovers']['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Options'),
    '#description' => t('These are global options. Each popover can independently override desired settings by appending the option name to !data. Example: !data-animation.', array(
      '!data' => '<code>data-</code>',
      '!data-animation' => '<code>data-animation="false"</code>',
    )),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="bootexcellence_popover_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['setexcellence']['tabs']['othercomponents']['popovers']['options']['excellence_popover_animation'] = array(
    '#type' => 'checkbox',
    '#title' => t('animate'),
    '#description' => t('Apply a CSS fade transition to the popover.'),
    '#default_value' => theme_get_setting('excellence_popover_animation', 'excellence_simple'),
  );
  $form['setexcellence']['tabs']['othercomponents']['popovers']['options']['excellence_popover_html'] = array(
    '#type' => 'checkbox',
    '#title' => t('HTML'),
    '#description' => t("Insert HTML into the popover. If false, jQuery's text method will be used to insert content into the DOM. Use text if you're worried about XSS attacks."),
    '#default_value' => theme_get_setting('excellence_popover_html', 'excellence_simple'),
  );
  $form['setexcellence']['tabs']['othercomponents']['popovers']['options']['excellence_popover_placement'] = array(
    '#type' => 'select',
    '#title' => t('placement'),
    '#description' => t('Where to position the popover. When "auto" is specified, it will dynamically reorient the popover. For example, if placement is "auto left", the popover will display to the left when possible, otherwise it will display right.'),
    '#default_value' => theme_get_setting('excellence_popover_placement', 'excellence_simple'),
    '#options' => drupal_map_assoc(array(
      'top',
      'bottom',
      'left',
      'right',
      'auto',
      'auto top',
      'auto bottom',
      'auto left',
      'auto right',
    )),
  );
  $form['setexcellence']['tabs']['othercomponents']['popovers']['options']['excellence_popover_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('selector'),
    '#description' => t('If a selector is provided, tooltip objects will be delegated to the specified targets. In practice, this is used to enable dynamic HTML content to have popovers added.'),
    '#default_value' => theme_get_setting('excellence_popover_selector', 'excellence_simple'),
  );
  $form['setexcellence']['tabs']['othercomponents']['popovers']['options']['excellence_popover_trigger'] = array(
    '#type' => 'checkboxes',
    '#title' => t('trigger'),
    '#description' => t('How a popover is triggered.'),
    '#default_value' => theme_get_setting('excellence_popover_trigger', 'excellence_simple'),
    '#options' => drupal_map_assoc(array(
      'click',
      'hover',
      'focus',
      'manual',
    )),
  );
  $form['setexcellence']['tabs']['othercomponents']['popovers']['options']['excellence_popover_trigger_autoclose'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto-close on document click'),
    '#description' => t('Will automatically close the current popover if a click occurs anywhere else other than the popover element.'),
    '#default_value' => theme_get_setting('excellence_popover_trigger_autoclose', 'excellence_simple'),
  );
  $form['setexcellence']['tabs']['othercomponents']['popovers']['options']['excellence_popover_title'] = array(
    '#type' => 'textfield',
    '#title' => t('title'),
    '#description' => t("Default title value if \"title\" attribute isn't present."),
    '#default_value' => theme_get_setting('excellence_popover_title', 'excellence_simple'),
  );
  $form['setexcellence']['tabs']['othercomponents']['popovers']['options']['excellence_popover_content'] = array(
    '#type' => 'textfield',
    '#title' => t('content'),
    '#description' => t('Default content value if "data-content" or "data-target" attributes are not present.'),
    '#default_value' => theme_get_setting('excellence_popover_content', 'excellence_simple'),
  );
  $form['setexcellence']['tabs']['othercomponents']['popovers']['options']['excellence_popover_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('delay'),
    '#description' => t('The amount of time to delay showing and hiding the popover (in milliseconds). Does not apply to manual trigger type.'),
    '#default_value' => theme_get_setting('excellence_popover_delay', 'excellence_simple'),
  );
  $form['setexcellence']['tabs']['othercomponents']['popovers']['options']['excellence_popover_container'] = array(
    '#type' => 'textfield',
    '#title' => t('container'),
    '#description' => t('Appends the popover to a specific element. Example: "body". This option is particularly useful in that it allows you to position the popover in the flow of the document near the triggering element - which will prevent the popover from floating away from the triggering element during a window resize.'),
    '#default_value' => theme_get_setting('excellence_popover_container', 'excellence_simple'),
  );


  $form['setexcellence']['tabs']['othercomponents']['tooltips']['excellence_tooltip_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable tooltips'),
    '#description' => t('Elements that have the !code attribute set will automatically initialize the tooltip upon page load. !warning', array(
      '!code' => '<code>data-toggle="tooltip"</code>',
      '!warning' => '<strong class="error text-error">WARNING: This feature can sometimes impact performance. Disable if pages appear to "hang" after initial load.</strong>',
    )),
    '#default_value' => theme_get_setting('excellence_tooltip_enabled', 'excellence_simple'),
  );
  $form['setexcellence']['tabs']['othercomponents']['tooltips']['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Options'),
    '#description' => t('These are global options. Each tooltip can independently override desired settings by appending the option name to !data. Example: !data-animation.', array(
      '!data' => '<code>data-</code>',
      '!data-animation' => '<code>data-animation="false"</code>',
    )),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="bootexcellence_tooltip_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['setexcellence']['tabs']['othercomponents']['tooltips']['options']['excellence_tooltip_animation'] = array(
    '#type' => 'checkbox',
    '#title' => t('animate'),
    '#description' => t('Apply a CSS fade transition to the tooltip.'),
    '#default_value' => theme_get_setting('excellence_tooltip_animation', 'excellence_simple'),
  );
  $form['setexcellence']['tabs']['othercomponents']['tooltips']['options']['excellence_tooltip_html'] = array(
    '#type' => 'checkbox',
    '#title' => t('HTML'),
    '#description' => t("Insert HTML into the tooltip. If false, jQuery's text method will be used to insert content into the DOM. Use text if you're worried about XSS attacks."),
    '#default_value' => theme_get_setting('excellence_tooltip_html', 'excellence_simple'),
  );
  $form['setexcellence']['tabs']['othercomponents']['tooltips']['options']['excellence_tooltip_placement'] = array(
    '#type' => 'select',
    '#title' => t('placement'),
    '#description' => t('Where to position the tooltip. When "auto" is specified, it will dynamically reorient the tooltip. For example, if placement is "auto left", the tooltip will display to the left when possible, otherwise it will display right.'),
    '#default_value' => theme_get_setting('excellence_tooltip_placement', 'excellence_simple'),
    '#options' => drupal_map_assoc(array(
      'top',
      'bottom',
      'left',
      'right',
      'auto',
      'auto top',
      'auto bottom',
      'auto left',
      'auto right',
    )),
  );
  $form['setexcellence']['tabs']['othercomponents']['tooltips']['options']['excellence_tooltip_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('selector'),
    '#description' => t('If a selector is provided, tooltip objects will be delegated to the specified targets.'),
    '#default_value' => theme_get_setting('excellence_tooltip_selector', 'excellence_simple'),
  );
  $form['setexcellence']['tabs']['othercomponents']['tooltips']['options']['excellence_tooltip_trigger'] = array(
    '#type' => 'checkboxes',
    '#title' => t('trigger'),
    '#description' => t('How a tooltip is triggered.'),
    '#default_value' => theme_get_setting('excellence_tooltip_trigger', 'excellence_simple'),
    '#options' => drupal_map_assoc(array(
      'click',
      'hover',
      'focus',
      'manual',
    )),
  );
  $form['setexcellence']['tabs']['othercomponents']['tooltips']['options']['excellence_tooltip_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('delay'),
    '#description' => t('The amount of time to delay showing and hiding the tooltip (in milliseconds). Does not apply to manual trigger type.'),
    '#default_value' => theme_get_setting('excellence_tooltip_delay', 'excellence_simple'),
  );
  $form['setexcellence']['tabs']['othercomponents']['tooltips']['options']['excellence_tooltip_container'] = array(
    '#type' => 'textfield',
    '#title' => t('container'),
    '#description' => t('Appends the tooltip to a specific element. Example: "body"'),
    '#default_value' => theme_get_setting('excellence_tooltip_container', 'excellence_simple'),
  );

}