<div id="toTop"><span class="glyphicon glyphicon-chevron-up"></span></div>
<header id="navbar" role="banner" class="navbar navbar-fixed-top container-fluid">
  <div class="container">
    <div class="navbar-header">
      <?php if ($logo): ?>
      <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
      <?php endif; ?>

      <?php if (!empty($site_name)): ?>
      <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
      <?php endif; ?>
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
      <div class="navbar-collapse collapse" id="page-template">
        <nav role="navigation">
          <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($secondary_nav)): ?>
            <?php print render($secondary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($page['navigation'])): ?>
            <?php print render($page['navigation']); ?>
          <?php endif; ?>
        </nav>
      </div>
    <?php endif; ?>
  </div>
</header>
<header id="header" role="banner" class="masthead clearfix">
  <div class="container">
    <div id="header-inside" class="clearfix">
      <div class="row  animated slideInDown">
        <div class="col-md-12">
          <?php if ($site_slogan):?>
          <div id="site-slogan">
            <?php print $site_slogan; ?>
          </div>
		   <?php if (!empty($search_box) && theme_get_setting('excellence_searchform_header')):?>
		   <div id="header-form-search">
		   <?php if (!empty($search_box)) { echo render($search_box); } ?>
		   </div>
		   <?php endif; ?>
          <?php endif; ?>
          <?php if ($page['header']) :?>
          <?php print render($page['header']); ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</header>
<?php if ($page['banner']) : ?>
<div id="banner" class="clearfix">
  <div class="container">
    <div id="banner-inside" class="clearfix">
      <div class="row animated slideInDown">
        <div class="col-md-12">
          <?php print render($page['banner']); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
<div id="page" class="sidebar-show clearfix"><div id="toToggle"><span class="glyphicon glyphicon-tasks"></span></div>
  <?php if ($page['highlighted']):?>
  <div id="top-content" class="clearfix">
    <div class="container">
      <div id="top-content-inside" class="clearfix">
        <div class="row animated slideInDown">
          <div class="col-md-12">
            <?php print render($page['highlighted']); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>
  <div id="main-content">
    <div class="container-fluid">
      <?php if ($messages):?>
      <div id="messages-console" class="clearfix">
        <div class="row">
          <div class="col-md-12">
            <?php print $messages; ?>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <div class="row">
        <?php if ($page['sidebar_first']):?>
        <aside class="<?php print $sidebar_grid_class; ?>">
          <section id="sidebar-first" class="sidebar clearfix">
            <div class="inner-container">
              <?php print render($page['sidebar_first']); ?>
            </div>
          </section>
        </aside>
        <?php endif; ?>
        <section id="template-page" class="sidebar-show <?php print $main_grid_class; ?>">
          <div class="inner-container">
            <div id="main" class="clearfix">
              <?php if (!empty($breadcrumb) && theme_get_setting('excellence_breadcrumb_display')):?>
              <div id="breadcrumb" class="clearfix">
                <div id="breadcrumb-inside" class="clearfix">
                  <?php print $breadcrumb; ?>
                </div>
              </div>
              <?php endif; ?>
              <?php if ($page['promoted']):?>
              <div id="promoted" class="clearfix">
                <div id="promoted-inside" class="clearfix">
                  <?php print render($page['promoted']); ?>
                </div>
              </div>
              <?php endif; ?>
              <div id="content-wrapper">
                <?php print render($title_prefix); ?>
                <?php if ($title):?>
                <h1 class="page-title"><?php print $title; ?></h1>
                <?php endif; ?>
                <?php print render($title_suffix); ?>
                <?php print render($page['help']); ?>
                <?php if ($tabs):?>
                <div class="tabs">
                  <?php print render($tabs); ?>
                </div>
                <?php endif; ?>
                <?php if ($action_links):?>
                <ul class="action-links">
                  <?php print render($action_links); ?>
                </ul>
                <?php endif; ?>
                <?php print render($page['content']); ?>
                <?php print $feed_icons; ?>
              </div>
            </div>
          </div>
        </section>
        <?php if ($page['sidebar_second']):?>
        <aside class="<?php print $sidebar_grid_class; ?>">
          <section id="sidebar-second" class="sidebar clearfix">
            <div class="inner-container">
              <?php print render($page['sidebar_second']); ?>
            </div>
          </section>
        </aside>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <?php if ($page['bottom_content']):?>
  <div id="bottom-content" class="clearfix">
    <div class="container">
      <div id="bottom-content-inside" class="clearfix">
        <div class="row animated slideInDown">
          <div class="col-md-12">
            <?php print render($page['bottom_content']); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>
</div>
<?php if ($page['footer_first'] || $page['footer_second'] || $page['footer_third'] || $page['footer_fourth']):?>
<footer id="footer" class="clearfix">
  <div class="container">
    <div id="footer-inside" class="clearfix">
      <div class="row">
        <div class="col-md-3">
          <?php if ($page['footer_first']):?>
          <div class="footer-area">
            <?php print render($page['footer_first']); ?>
          </div>
          <?php endif; ?>
        </div>
        <div class="col-md-3">
          <?php if ($page['footer_second']):?>
          <div class="footer-area">
            <?php print render($page['footer_second']); ?>
          </div>
          <?php endif; ?>
        </div>
        <div class="col-md-3">
          <?php if ($page['footer_third']):?>
          <div class="footer-area">
            <?php print render($page['footer_third']); ?>
          </div>
          <?php endif; ?>
        </div>
        <div class="col-md-3">
          <?php if ($page['footer_fourth']):?>
          <div class="footer-area">
            <?php print render($page['footer_fourth']); ?>
          </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</footer>
<?php endif; ?>
<footer id="subfooter" class="clearfix">
  <div class="container">
    <div id="subfooter-inside" class="clearfix">
      <div class="row">
        <div class="col-md-12">
          <div class="subfooter-area">
            <?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('class' => array('menu', 'secondary-menu', 'links', 'clearfix')))); ?>
            <?php if ($page['footer']):?>
            <?php print render($page['footer']); ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>