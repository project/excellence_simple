(function ($) {
    "use strict"; // Start of use strict
	//<div id="menu_toggle" style="display: block;"><span class="glyphicon glyphicon-chevron-up"></span></div>
    $('#toToggle').on('click', function() {
	if ($('#template-page').hasClass('sidebar-show')) {
		// hide sidebar when click glyphicon
		$('#sidebar-first').hide();
		// espand if hasclass col-md-8
		$('#template-page').addClass('col-md-12').removeClass('sidebar-show col-md-8');
	} else {
		$('#sidebar-first').show();
		$('#template-page').addClass('sidebar-show col-md-8').removeClass('col-md-12');
		}
    });
    // Closes responsive menu when a scroll trigger link is clicked
    $(".js-scroll-trigger").click(function () {
        $(".navbar-collapse").collapse("hide");
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $("body").scrollspy({
        target: "#navbar",
        offset: 74,
    });

    // Collapse Navbar
    var navbarCollapse = function () {
        if ($("#navbar").offset().top > 100) {
            $("#navbar").addClass("navbar-shrink");
        } else {
            $("#navbar").removeClass("navbar-shrink");
        }
    };
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);

    // scroll top
    $(window).scroll(function() {
		if($(this).scrollTop() != 0) {
			$("#toTop").fadeIn();
		} else {
			$("#toTop").fadeOut();
		}
	});

	$("#toTop").click(function() {
		$("body,html").animate({scrollTop:0},800);
	});

})(jQuery);
