(function ($, Drupal) {
  /*global jQuery:false */
  /*global Drupal:false */
  "use strict";

  /**
   * Provide vertical tab summaries for Bootstrap settings.
   */
  Drupal.behaviors.bootexcellenceSettingSummaries = {
    attach: function (context) {
      var $context = $(context);

      // General.
      $context.find('#edit-setexcellence').drupalSetSummary(function () {
        var summary = [];
        // Buttons.
        var size = $context.find('select[name="bootexcellence_button_size"] :selected');
        if (size.val()) {
          summary.push(Drupal.t('@size Buttons', {
            '@size': size.text()
          }));
        }

        // Images.
        var shape = $context.find('select[name="bootexcellence_image_shape"] :selected');
        if (shape.val()) {
          summary.push(Drupal.t('@shape Images', {
            '@shape': shape.text()
          }));
        }
        if ($context.find(':input[name="bootexcellence_image_responsive"]').is(':checked')) {
          summary.push(Drupal.t('Responsive Images'));
        }

        // Tables.
        if ($context.find(':input[name="bootexcellence_table_responsive"]').is(':checked')) {
          summary.push(Drupal.t('Responsive Tables'));
        }

        return summary.join(', ');


        var summary = [];
        // Breadcrumbs.
        var breadcrumb = parseInt($context.find('select[name="bootexcellence_breadcrumb"]').val(), 10);
        if (breadcrumb) {
          summary.push(Drupal.t('Breadcrumbs'));
        }        
        return summary.join(', ');

        var summary = [];
        if ($context.find('input[name="bootexcellence_anchors_fix"]').is(':checked')) {
          summary.push(Drupal.t('Anchors'));
        }
        if ($context.find('input[name="bootexcellence_popover_enabled"]').is(':checked')) {
          summary.push(Drupal.t('Popovers'));
        }
        if ($context.find('input[name="bootexcellence_tooltip_enabled"]').is(':checked')) {
          summary.push(Drupal.t('Tooltips'));
        }
        return summary.join(', ');
      });

    }
  };

})(jQuery, Drupal);
